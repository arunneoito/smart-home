FirebaseData firebaseData;



void fetchInitial(String path){
 bool ext=true; 
 
     while(ext){
    if (!Firebase.beginStream(firebaseData, path))
    {
      Serial.println("------------------------------------");
      Serial.println("Can't begin stream connection...");
      Serial.println("REASON: " + firebaseData.errorReason());
      Serial.println("------------------------------------");
      Serial.println();
      ext=false;
    }
        if(!Firebase.readStream(firebaseData)){
           Serial.println("Read Stream Error");
       }
  
       
    if (firebaseData.streamAvailable())
    {
      FirebaseJson &json = firebaseData.jsonObject();
     String jsonStr;
     json.toString(jsonStr, true);
     Serial.println(jsonStr);
     size_t len = json.iteratorBegin();
      String key, value = "";
      int type = 0;
      int pin;
          for (size_t i = 0; i < len; i++)
      {
      json.iteratorGet(i, type, key, value);
      if(key.toInt()>0){
        pinMode(key.toInt(),OUTPUT);
        pin=key.toInt();}
      else if(key=="value"){
        if(value=="true"){
        digitalWrite(pin,true);}
        else if(value=="false"){
          digitalWrite(pin,false);
          }
        }
   }      
   json.iteratorEnd();
    ext=false;
  }

}}
void streamer(){
  if(!Firebase.readStream(firebaseData)){
       Serial.print("connectionlost");
       while(checkConnect()){
       digitalWrite(2,HIGH);
       delay(1000);
       digitalWrite(2,LOW);
       delay(300);
       }
       ESP.restart();
       }
         if(firebaseData.streamAvailable()){
        printResult(firebaseData);
       
   
    }
  }
  void printResult(FirebaseData &data)
  {
     FirebaseJson &json = firebaseData.jsonObject();
     String jsonStr;
     json.toString(jsonStr, true);
     size_t len = json.iteratorBegin();
     String datapath=firebaseData.dataPath();
      datapath.remove(0,1);
      Serial.println(datapath);
      String key, value = "";
      int type = 0;
      int pin=datapath.toInt();
                for (size_t i = 0; i < len; i++)
      {
      json.iteratorGet(i, type, key, value);
      if(key=="value"){
        if(value=="false")digitalWrite(pin,LOW);
        else if(value=="true")digitalWrite(pin,HIGH);
        }
   }  
         
  }
  bool checkConnect(){
    for(int i=0;i<20;i++){
      if(WiFi.status()!= 3){
        Serial.println("connection lost");
        return true;}
        delay(200);
      }
      return false;
    }
