  #include <ArduinoJson.h>

#include <Arduino.h>



#include "FirebaseESP8266.h"
#include <ESP8266WiFi.h>
#include <Arduino.h>
#ifdef ESP32
#include <WiFi.h>
#include <AsyncTCP.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <EEPROM.h>
#endif
#include <ESPAsyncWebServer.h>

#define FIREBASE_HOST "YOUR FIREBASE DATABASE URL"
#define FIREBASE_AUTH "YOUR FIRBASE DATABASE SECRET KEY"

#define APSSID "WIFI SSID TO CONFIG DEVICE"
#define APPSK  "WIFI PASSWORD TO CONFIG DEVICE"

AsyncWebServer server(80);
String ssid;
String password;
String path;

void setup() {
    Serial.begin(115200);
    EEPROM.begin(512);
    pinMode(2,OUTPUT);
    pinMode(4,INPUT_PULLUP);
    pinMode(9,INPUT);
    int configu=digitalRead(4);
    if(!configu){
    configure();
    while(configu){
      digitalWrite(2,HIGH);
      delay(500);
      digitalWrite(2,LOW);
      delay(250);
     }
    }
    else{
      WiFi.mode(WIFI_STA);
       Serial.println("Normal Mode");
       String UID=read_String(5);
       String section=read_String(50);
       ssid=read_String(80);
       password=read_String(120);
       Serial.println("Connecting to :- "+ssid);
       WiFi.begin(ssid,password);
       Serial.print("Connecting to Wi-Fi");
       digitalWrite(2,LOW);
       while (WiFi.status() != WL_CONNECTED)
       {
       Serial.print(".");
       delay(300);
       }
       digitalWrite(2,HIGH);
       Serial.println("connected");
       FirebaseData firebaseData;
       Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
       Firebase.reconnectWiFi(true);
       firebaseData.setBSSLBufferSize(1024, 1024);
       firebaseData.setResponseSize(1024);
       path = "/users/"+UID+"/controls/"+section;
       Serial.println(path);
       fetchInitial(path);
    }
}

void loop(){
 streamer();
}           
