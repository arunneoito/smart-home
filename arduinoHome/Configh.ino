const char PAGE_Index[] PROGMEM = R"=====(
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <style>
    .forum {
      display: flex;
      justify-content: center;
      margin: 0px auto;
      background-color: rgba(31, 32, 32, 0.822);
      width: 300px;
      border-radius: 10px;
    }
    .forum input[type="text"],[type="password"] {
      margin: 10px;
      padding: 5px;
      border-radius: 8px;
      border: none;
    }
    .forum button {
      background-color: rgba(31, 32, 32, 0.822);
      color: cornsilk;
      padding: 4px;
      border-radius: 5px;
    }
    .forum p {
      color: red;
      font-family: Cambria, Cochin, Georgia, Times, "Times New Roman", serif;
    }
  </style>
  <body style="background-color: darkgray;">
    <center><h2>REGISTER USERID</h2></center>
    <div class="forum">
      <div class="input">
        <input type="text" id="usr" placeholder="UserID" />
        <input type="text" id="Section" placeholder="Section" />
        <input type="text" id="ssid" placeholder="WIFI SSID"/>
        <input type="password" id="password" placeholder="Password"/> 
        <button id="btn">Register</button>
      </div><br>
      <p id="error"></p>
    </div>
  </body>
  <script>
    var usrid = document.getElementById("usr");
    var sect = document.getElementById("Section");
    var SSID = document.getElementById("ssid");
    var password = document.getElementById("password");
    var btn = document.getElementById("btn");
    var err = document.getElementById("error");
    btn.onclick = () => {
      if (usrid.value!=="" && sect.value!="" && SSID.value!="") {
        fetch("/post",{
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ username: usrid.value,
             section: sect.value,
             ssid:SSID.value,
             pass:password.value})
        })
          .then(res => {
            console.log(res);
            err.innerHTML = "";
            usrid.value = "";
            sect.value = "";
            SSID.value = "";
            password.value = "";
            err.innerHTML="Registered Succesfully";
          })
          .catch(error => {
            copnsole.log(error);
            err.innerHTML = "Invalid Entries";
          });
      } else {
        err.innerHTML = "Invalid Entries";
      }
    };
  </script>
</html>


)=====";

const char* PARAM_MESSAGE="message";

void notFound(AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "Not found");
}


void configure(){
  Serial.println("Configuration Mode");
  WiFi.mode(WIFI_AP);
    WiFi.softAP(APSSID,APPSK);
    Serial.println(WiFi.softAPIP());
    delay(50);
     server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(200, "text/html",PAGE_Index);
    });

    
     server.on("/get", HTTP_GET, [] (AsyncWebServerRequest *request) {
        String message="*";
        if (request->hasParam(PARAM_MESSAGE)) {
            message = request->getParam(PARAM_MESSAGE)->value();
            writeString(10,message); 
            Serial.println(message);
        } else {
            message = "No message sent";
            Serial.println(message);
        }
        request->send(200, "text/plain", "Hello, GET: " + message);
    });
      server.on("/post",HTTP_POST,[](AsyncWebServerRequest * request){},NULL,[](AsyncWebServerRequest * request, uint8_t *data, size_t len, size_t index, size_t total) {
      String body; 
      for (size_t i = 0; i < len; i++) {
       body+=(char)data[i];
      }
      StaticJsonBuffer<200> jsonBuffer;
      JsonObject& root = jsonBuffer.parseObject(body);
      String usrid=root["username"];
      String section=root["section"]; 
      String ssid=root["ssid"];
      String pass=root["pass"];
      writeString(5,usrid);
      writeString(50,section);
      writeString(80,ssid);
      writeString(120,pass);
      request->send(200);
  });

    server.onNotFound(notFound);
    server.begin();}
    void writeString(char add,String data)
{
  int _size = data.length()+1;
  String dat="*";
  dat+=data;
  int i;
  for(i=0;i<_size;i++)
  {
    EEPROM.write(add+i,dat[i]);
  }
  EEPROM.write(add+_size,'\0');
  EEPROM.commit();
}

String read_String(char add)
{
  int i;
  char data[100];
  int len=0;
  unsigned char k;
  k=EEPROM.read(add);
  if(k==42){
  while(k != '\0' && len<100)
  {    
    k=EEPROM.read((add+1)+len);
    data[len]=k;  
    len++;
  }
  data[len]='\0';
  return String(data);
  }return "";
}
